<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials/header')
    @yield('css')
</head>
<body>
    @include('partials/navbar')
    @include('partials/sidebar')
    <div class="main">
        <div class="container">
            @yield('content')
        </div>
    </div>
    @include('partials/footer')
    @yield('script')
</body>
</html>