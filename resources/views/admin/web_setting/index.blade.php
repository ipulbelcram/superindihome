@extends('dashboardTemplate')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h5>sliders</h5><hr>
            @include('partials/flash_message')
        </div>
        
        <div class="col-md-12">
            {!! Form::open(['route' => 'web_setting.store']) !!}
                @include('admin/web_setting/form', ['button' => 'Simpan'])
            {!! Form::close() !!}
        </div>
    </div>
    @include('partials/modalDelete', ['route' => 'sliders.destroy'])
@endsection
@section('script')
@endsection