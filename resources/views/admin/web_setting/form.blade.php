<div>
    
    <div class="form-group">
        <h6 class="card-title">Contact</h6>
        @php if($errors->has('contact')) { $errorContact = 'is-invalid'; } else {$errorContact = ''; } @endphp
        {!! Form::number('contact', !empty($contact) ? $contact->param : '', ["class" => "form-control $errorContact"]) !!}
        @error('contact')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <h6 class="card-title">Email</h6>
        @php if($errors->has('email')) { $errorEmail = 'is-invalid'; } else {$errorEmail = ''; } @endphp
        {!! Form::email('email', !empty($email) ? $email->param : '', ["class" => "form-control $errorEmail"]) !!}
        @error('email')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <h6 class="card-title">Address</h6>
        @php if($errors->has('address')) { $errorAddress = 'is-invalid'; } else {$errorAddress = ''; } @endphp
        {!! Form::textarea('address', !empty($address) ? $address->param : '', ["class" => "form-control $errorAddress"]) !!}
        @error('address')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary btn-block">Submit</button>
</div>