@extends('dashboardTemplate')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h5>Packages</h5><hr>
            @include('partials/flash_message')
        </div>
        
        <div class="col-md-12">
            <div class="row row-cols-1 row-cols-md-2">
                <div class="col-lg-4 col-md-6 mb-3">
                    <a href="{{ route('packages.create') }}">
                        <div class="card text-center py-4" style="height:100%">
                            <div class="card-body">
                                <span class="mdi mdi-plus mdi-48px"></span>
                                <h6>Tambah Package</h6>
                                <h5 class="card-title"></h5>
                            </div>
                        </div>
                    </a>
                </div>
                @foreach($data_package as $package)
                <div class="col-lg-4 mb-3">
                    <a href="{{ route('packages.edit', [$package->slug]) }}">
                        <div class="card">
                            <div class="position-relative">
                                <img src="{{ asset('image/packages/'.$package->image) }}" class="card-img-top" alt="...">
                                @if($package->active == 'true')
                                    <div class="position-absolute" style="right:10px; top:10px;"><span class="badge badge-pill badge-primary">Active</span></div>
                                @else
                                    <div class="position-absolute" style="right:10px; top:10px;"><span class="badge badge-pill badge-danger">Not Active</span></div>
                                @endif
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{ $package->title }}</h5>
                                <div class="float-right">
                                    <a  data-toggle="modal" data-target="#ModalDelete" data-id="{{ $package->id }}" id="hapusData"><button class="btn btn-danger btn-sm">Hapus</button></a>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @include('partials/modalDelete', ['route' => 'packages.destroy'])
@endsection