<div>
    <div class="text-center">
        <img src="{{ asset(!empty(($package->image)) ? 'image/packages/'.$package->image : 'image/blank.png') }}" class="img-fluid" id="preview-image">
        <div class="form-group py-3">
            <input name="image" id="image-input" type="file" accept='image/*'>
            @error('image')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <h6 class="card-title">Title</h6>
        @php if($errors->has('title')) { $errorTitle = 'is-invalid'; } else {$errorTitle = ''; } @endphp
        {!! Form::text('title', null, ["class" => "form-control $errorTitle"]) !!}
        @error('title')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <h6 class="card-title">Package Name</h6>
        @php if($errors->has('package_name')) { $errorPackageName = 'is-invalid'; } else {$errorPackageName = ''; } @endphp
        {!! Form::text('package_name', null, ["class" => "form-control $errorPackageName"]) !!}
        @error('package_name')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <h6 class="card-title">Content</h6>
        {!! Form::textarea('content', null) !!}
        @error('content')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-group">
        <h6 class="card-title">Active</h6>
        @php if($errors->has('active')) { $errorActive = 'is-invalid'; } else {$errorActive = ''; } @endphp
        {!! Form::select('active', ['true' => 'Active', 'false' => 'Not Active'], '', ['class' => 'form-control $errorActive']) !!}
        @error('active')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary btn-block">Submit</button>
</div>