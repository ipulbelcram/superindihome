@extends('layouts/frondendTemplate')
@section('content')
    <section id="content" class="clearfix mt-5 pt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 text-left">
                    <h2 class="font-weight-bold m-0" id="title">{{ $package->title }}</h2>
                    <small class="date">Super Indihome | <font id="date">{{ $package->updated_at }}</font></small>
                    
                    <div id="image">
                        <img src="{{ asset('image/packages/'.$package->image) }}" class="img-fluid">
                    </div>
                    
                    <div id="description" class="mt-5">
                    {!! $package->content !!}
                    </div>
                    
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            {!! Form::open(['route' => 'forms.store']) !!}
                                <h3>Formulir Pendaftaran</h3>
                                <div class="form-group">
                                    <label class="font-weight-bold">PAKET YANG ANDA PILIH</label>
                                    <input type="text" name="package_name" value="{{ $package->title }}" class="form-control">
                                    <input type="hidden" name="package_id" class="form-control" value="{{ $package->id }}">
                                </div>
    
                                <div class="form-group">
                                    <label class="font-weight-bold">NAMA</label>
                                    <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror">
                                    @error('nama')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
    
                                <div class="form-group">
                                    <label class="font-weight-bold">EMAIL</label>
                                    <input type="text" name="email" class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
    
                                <div class="form-group">
                                    <label class="font-weight-bold">NO. HANDPHONE</label>
                                    <input type="text" name="no_hp" class="form-control @error('no_hp') is-invalid @enderror">
                                    @error('no_hp')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                    <p style="font-size:12px;">
                                        Pastikan nomor Anda aktif agar dapat memudahkan petugas kami menghubungi Anda.
                                    </p>
                                </div>
    
                                <div class="form-group">
                                    <label class="font-weight-bold">PROVINSI</label>
                                    <select name="provinsi_id" id="provinsi" class="form-control @error('provinsi_id') is-invalid @enderror">
                                        <option value="">-- Pilih Provinsi --</option>
                                    </select>
                                    @error('provinsi_id')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
    
                                <div class="form-group">
                                    <label class="font-weight-bold">Kota</label>
                                    <select name="kab_kota_id" id="kab_kota" class="form-control @error('kab_kota_id') is-invalid @enderror">
                                        <option value="">-- Pilih Kota --</option>
                                    </select>
                                    @error('kab_kota_id')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
    
                                <div class="form-group">
                                    <label class="font-weight-bold">ALAMAT PEMASANG</label>
                                    <textarea name="alamat_pemasangan" rows="5" class="form-control @error('alamat_pemasangan') is-invalid @enderror"></textarea>
                                    @error('alamat_pemasangan')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
    
                                <button class="btn btn-danger btn-block">Daftar</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $("#provinsi option:first").prop('disabled',true);
            $("#kab_kota option:first").prop('disabled',true);
            $("#kab_kota").prop("disabled", true);

            $.ajax({
                url: "http://superindihome.com/api/get_provinsi",
                type: "GET",
                dataType: "JSON",
                success: function(result) {
                    let data = result.data

                    $.each(data, function(key, value) {
                        let append = "<option value="+ value.id +">"+value.provinsi+"</option>"
                        $("#provinsi").append(append)
                    })
                }
            })

            $("#provinsi").change(function() {
                let provinsiVal = $("#provinsi").val();
                $.ajax({
                    url: "http://superindihome.com/api/get_kab_kota/"+provinsiVal,
                    type: "GET",
                    dataType: "JSON",
                    success: function(result) {
                        let data = result.data
                        $("#kab_kota").prop("disabled", false)
                        $("#kab_kota").html("")
                        $.each(data, function(key, value) {
                            let append = "<option value="+ value.id +">"+value.kab_kota+"</option>"
                            $("#kab_kota").append(append)
                        })
                    }
                })
            })
        })
    </script>
@endsection

