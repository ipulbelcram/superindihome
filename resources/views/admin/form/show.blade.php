@extends('dashboardTemplate')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h5>Show Forms</h5><hr>
            <!-- @include('partials/flash_message') -->
        </div>

        <div class="col-md-12">
            <table class="table-borderless">
                    <tr>
                        <th>Package Name</th>
                        <th class="px-3">:</th>
                        <th>{{ $form->packages->package_name }}</th>
                    </tr>
                    <tr>
                        <th>Nama Pemesan</th>
                        <th class="px-3">:</th>
                        <th>{{ $form->nama }}</th>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <th class="px-3">:</th>
                        <th>{{ $form->email }}</th>
                    </tr>
                    <tr>
                        <th>No.Hp</th>
                        <th class="px-3">:</th>
                        <th>{{ $form->no_hp }}</th>
                    </tr>
                    <tr>
                        <th>Provinsi</th>
                        <th class="px-3">:</th>
                        <th>{{ $form->provinsi->provinsi }}</th>
                    </tr>
                    <tr>
                        <th>Kota</th>
                        <th class="px-3">:</th>
                        <th>{{ $form->kab_kota->kab_kota }}</th>
                    </tr>
                    <tr>
                        <th>Alamat Pemasangan</th>
                        <th class="px-3">:</th>
                        <th>{{ $form->alamat_pemasangan }}</th>
                    </tr>
                    <tr>
                        <th>Pesan di buat</th>
                        <th class="px-3">:</th>
                        <th>{{ $form->created_at }}</th>
                    </tr>
            </table>
        </div>
    </div>
@endsection