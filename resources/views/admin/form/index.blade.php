@extends('dashboardTemplate')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h5>Formulir</h5><hr>
            @include('partials/flash_message')
        </div>

        <div class="col-md-12">
            <table class="table table-hover">
                <thead>
                    <th scope="col">#</th>
                    <th scope="col">Paket</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">No.HP</th>
                    <th></th>
                </thead>
                <tbody>
                    <?php $no=1; ?>
                    @foreach($data_forms as $forms)
                        <tr>
                            <td scope="row">{{ $no++ }}</td>
                            <td>{{ $forms->packages->package_name }}</td>
                            <td>{{ $forms->nama }}</td>
                            <td>{{ $forms->email }}</td>
                            <td>{{ $forms->no_hp }}</td>
                            <td>
                                <a href="{{ route('forms.show', [$forms->id]) }}"><button class="btn btn-sm btn-primary">Lihat Detail</button></a>
                                <a  data-toggle="modal" data-target="#ModalDelete" data-id="{{ $forms->id }}" id="hapusData"><button class="btn btn-danger btn-sm">Hapus</button></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $data_forms->links() }}
        </div>
    </div>
    @include('partials/modalDelete', ['route' => 'forms.destroy'])
@endsection