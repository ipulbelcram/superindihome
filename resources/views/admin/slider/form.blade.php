<div>
    <div class="text-center">
        <img src="{{ asset(!empty(($slider->image)) ? 'image/sliders/'.$slider->image : 'image/blank.png') }}" class="img-fluid" id="preview-image">
        <div class="form-group py-3">
            <input name="image" id="image-input" type="file" accept='image/*'>
            @error('image')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <h6 class="card-title">Title</h6>
        @php if($errors->has('title')) { $errorTitle = 'is-invalid'; } else {$errorTitle = ''; } @endphp
        {!! Form::text('title', null, ["class" => "form-control $errorTitle"]) !!}
        @error('title')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <h6 class="card-title">Order</h6>
        @php if($errors->has('order')) { $errorOrder = 'is-invalid'; } else {$errorOrder = ''; } @endphp
        {!! Form::number('order', null, ["class" => "form-control $errorOrder"]) !!}
        @error('order')
            <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary btn-block">Submit</button>
</div>