@extends('dashboardTemplate')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h5>slider Form Edit</h5><hr>
        </div>

        <div class="col-lg-12 d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        {!! Form::model($slider, ['method' => 'patch', 'route' => ['sliders.update', $slider->id], 'files' => true]) !!}
                            @include('admin/slider/form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/vendors/ckeditor_full/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.replace( 'content' );

        $("#image-input").change(function() {
            readURL(this);
        })

        function readURL(input) {
            if(input.files && input.files[0]) {
                var reader = new FileReader()

                reader.onload = function(e) {
                    $("#preview-image").attr("src", e.target.result)
                }

                reader.readAsDataURL(input.files[0]) // Convert to base 64 string
            }

        }
    </script>
@endsection