	<div class="modal fade" id="modalUploadDocument" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header border-bottom-0">
					<h5 class="modal-title" id="exampleModalLongTitle">Choose document workflow</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body mx-1 mb-2">
					<div class="row">
						<div class="col">
							<a href="{{asset('/upload')}}" class="text-dark">
								<div class="card text-center">
									<i class="mdi mdi-fingerprint mdi-48px"></i>
									<h6>Sign & Share</h6>
								</div>
							</a>
						</div>
						<div class="col">
							<a href="{{asset('/upload')}}" class="text-dark">
								<div class="card text-center">
									<i class="mdi mdi-account-group mdi-48px"></i>
									<h6>Share Only</h6>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{asset('/assets/vendors/jquery/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/vendors/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/js/main.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/js/alert.js')}}"></script>
	<script>
		$(document).ready(function() {
            $('#ModalDelete').on('show.bs.modal', function(e) {
                var id = $(e.relatedTarget).data('id');
                $(this).find('.modal-body #id').val(id);
            });
        })
	</script>