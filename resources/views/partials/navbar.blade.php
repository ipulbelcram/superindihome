    <nav class="navbar navbar-expand-sm navbar-light bg-white border-bottom">
        <div class="form-inline">
            <i class="mdi mdi-menu mdi-24px d-block d-lg-none pointer text-dark mr-2" id="menu"></i>
            <a class="navbar-brand" href="/dashboard">Prakerja</a>
        </div>
        <div class="dropdown ml-auto">
            <a href="#" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="mdi mdi-account-circle-outline text-dark mdi-24px"></i>
            </a>
            <div class="dropdown-menu rounded" aria-labelledby="dropdownMenu">
                <a class="dropdown-item" href="/profil">
                    <i class="mdi mdi-18px mdi-account-circle-outline"></i><span>Profil</span>
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item text-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="mdi mdi-18px mdi-login-variant"></i><span>{{ __('Logout') }}</span>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </a>
            </div>
        </div>
    </nav>