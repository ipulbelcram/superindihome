
<!-- <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Indihome</title>
    <link rel="stylesheet" href="{{asset('assets/vendors/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/vendors/mdi/css/materialdesignicons.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
</head>
<body> -->
    @extends('layouts/frondendTemplate')
    @section('content')
    <div class="container clearfix mt-5 pt-5 mb-5">
        <div class="row justify-content-center mt-4">
            <div class="col-lg-5">
                <div class="card">
                    <div class="card-body text-center">
                        <h5>Terimakasih</h5>
                        <p>Permintaan kamu sedang kami proses. Pastikan selalu hp kamu aktif agar dapat tim kami hubungi</p>
                        <a href="{{ route('home') }}"><button class="btn btn-danger">Kembali ke beranda</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    <!-- <script type="text/javascript" src="{{asset('/assets/vendors/jquery/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/vendors/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/js/main.js')}}"></script>
	<script type="text/javascript" src="{{asset('/assets/js/alert.js')}}"></script> -->
</body>
</html>