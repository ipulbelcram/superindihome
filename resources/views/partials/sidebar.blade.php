	<div class="sidebar">
		<a href="{{ route('dashboard') }}" class="{{($halaman == 'dashboard') ? 'active' : ''}}">
			<i class="mdi mdi-apps mdi-18px"></i><span>Dashboard</span>
		</a>
		<a href="{{ route('web_setting.index') }}" class="{{($halaman == 'web_setting') ? 'active' : ''}}">
			<i class="mdi mdi-map-marker-outline mdi-18px"></i><span>Web Setting</span>
		</a>

		<a href="{{ route('sliders.index') }}" class="{{($halaman == 'sliders') ? 'active' : ''}}">
			<i class="mdi mdi-map-marker-outline mdi-18px"></i><span>Slider</span>
		</a>

		<a href="{{ route('packages.index') }}" class="{{($halaman == 'packages') ? 'active' : ''}}">
			<i class="mdi mdi-map-marker-outline mdi-18px"></i><span>Package</span>
		</a>

		<a href="{{ route('forms.index') }}" class="{{($halaman == 'forms') ? 'active' : ''}}">
			<i class="mdi mdi-map-marker-outline mdi-18px"></i><span>Formulir</span>
		</a>
	</div>
	<div class="overlay"></div>