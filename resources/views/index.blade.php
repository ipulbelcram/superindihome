
@extends('layouts/frondendTemplate')
  @section('content')
    <!--==========================
      Intro Section
    ============================-->
    <section id="slider" class="clearfix mt-5 pt-5">
      <div class="banner">
            <div class="owl-carousel owl-theme pt-2">
                @foreach($data_slider as $slider)
                <div class="item" >
                    <img src="{{ asset('image/sliders/'.$slider->image) }}" >
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- #intro -->

    <main id="main">

      <!--==========================
        About Us Section
      ============================-->
      <section id="about">
        <div class="container">

          <header class="section-header">
            <h3>Tentang Kami</h3>
            <p>Sekilas Tentag Kami SuperIndihome</p>
          </header>

          <div class="row about-container">

            <div class="col-lg-6 content order-lg-1 order-2">
              <p>
                Indihome adalah sebuah internet rumahan unlimited yang bisa Anda gunakan untuk mengakses internet tanpa batas mulai dari youtube, instagram, browsing dan lain sebagainya. Dijamin Anda akan puas menggunakan internet yang kami sediakan
              </p>
              <p>
                Kami merupakan salah satu dari team marketing Indihome yang siap membantu Anda dalam memberikan pelayanan dari Indihome yaitu untuk pendaftaran Indihome. Dengan harga yang terjangkau, kami menawarakan berbagai macam paket yang dapat Anda pilih sesuai dengan keinginan Anda. Proses yang cepat dan mudah akan dibantu langsung oleh team marketing kami sehingga Anda tidak perlu menunggu lama.
              </p>
              <p>
                Untuk area pelayanan kami meliputi area Jakarta, Depok, Tangerang, dan Bogor. Berbagai macam promo menarik dapat Anda dapatkan jika menghubungi kami. Dengan Anda menghubungi kami, Anda tidak perlu datang ke kantor pusat Indihome sehingga proses akan lebih mudah dan akan mendapatkan promo terbaiknya
              </p>
            </div>

            <div class="col-lg-6 background order-lg-2 order-1 wow fadeInUp">
              <img src="{{ asset('image/tentang_kami.jpg') }}" class="img-fluid" alt="">
            </div>
          </div>

          <div class="row about-extra">
            <div class="col-lg-6 wow fadeInUp">
              <img src="{{ asset('image/home1.jpg') }}" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6 wow fadeInUp pt-5 pt-lg-0">
              <h4>Selamat datang di website superindihome</h4>
              <p>
                Indihome Jabodetabek melayani pemasangan indihome dengan berbagai pilihan paket seperti paket Fit Movie, Paket Prestige, Paket Streamix, Paket Phoenix dan Paket Gamer. Kami juga menyediakan banyak promo untuk Anda, jika Anda berminat silakan hubungi kontak kami dibawah ini.
              </p>
            </div>
          </div>

        </div>
      </section><!-- #about -->

      <!--==========================
        Services Section
      ============================-->
      <section id="packages" class="section-bg">
        <div class="container">

          <header class="section-header">
            <h3>Pilihan Paket</h3>
            <p>Yukk pilih pake sesuai kebutuhan Anda</p>
          </header>

          <div class="row">
            @foreach($data_package as $package)
            <div class="col-md-6 col-lg-5 offset-lg-1 wow bounceInUp" data-wow-duration="1.4s">
              <a href="{{ route('packages.show', [$package->slug]) }}">
                <div class="box mb-3">
                  <img src="{{ asset('image/packages/'.$package->image) }}" class="img-fluid">
                </div>
              </a>
            </div>
            @endforeach
          </div>

        </div>
      </section><!-- #services -->

      <!--==========================
        Why Us Section
      ============================-->
      <section id="why-us" class="wow fadeIn">
        <div class="container">
          <header class="section-header">
            <h3>Kenapa Memilih Kami</h3>
          </header>

          <div class="row row-eq-height justify-content-center">

            <div class="col-lg-3 mb-4">
              <div class="card wow bounceInUp">
                  <i class="fa fa-wifi icon"></i>
                <div class="card-body">
                  <h5 class="card-title">Koneksi Cepat</h5>
                </div>
              </div>
            </div>

            <div class="col-lg-3 mb-4">
              <div class="card wow bounceInUp">
                  <i class="fa fa-money icon"></i>
                <div class="card-body">
                  <h5 class="card-title">Harga Ekonomis</h5>
                </div>
              </div>
            </div>

            <div class="col-lg-3 mb-4">
              <div class="card wow bounceInUp">
                  <i class="fa fa-thumbs-o-up icon"></i>
                <div class="card-body">
                  <h5 class="card-title">Promo Menarik</h5>
                </div>
              </div>
            </div>

            <div class="col-lg-3 mb-4">
              <div class="card wow bounceInUp">
                  <i class="fa fa-users icon"></i>
                <div class="card-body">
                  <h5 class="card-title">Pelayanan Cepat</h5>
                </div>
              </div>
            </div>

          </div>

        </div>
      </section>

      <!--==========================
        Contact Section
      ============================-->
      <section id="contact">
        <div class="container-fluid">

          <div class="section-header">
            <h3>Kontak Kami</h3>
          </div>

          <div class="row wow fadeInUp">

            <div class="col-lg-6">
              <div class="map mb-4 mb-lg-0">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 312px;" allowfullscreen></iframe>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="row">
                <div class="col-md-5 info">
                  <i class="ion-ios-location-outline"></i>
                  <p>{{ $address }}</p>
                </div>
                <div class="col-md-4 info">
                  <i class="ion-ios-email-outline"></i>
                  <p>{{ $email }}</p>
                </div>
                <div class="col-md-3 info">
                  <i class="ion-ios-telephone-outline"></i>
                  <p>{{ $contact }}</p>
                </div>
              </div>
              
              <div class="form">
                {!! Form::open(['route' => 'sendMail', 'method' => 'post', 'role' => 'form']) !!}
                  <div class="form-row">
                    <div class="form-group col-lg-6">
                      <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                      <div class="validation"></div>
                      @error('name')
                        <div class="text-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <div class="form-group col-lg-6">
                      <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                      <div class="validation"></div>
                      @error('email')
                        <div class="text-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                    <div class="validation"></div>
                      @error('subject')
                        <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>
                  
                  <div class="form-group">
                    <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                    <div class="validation"></div>
                      @error('message')
                        <div class="text-danger">{{ $message }}</div>
                      @enderror
                  </div>

                  <div class="text-center">
                    <button type="submit" title="Send Message">Send Message</button>
                  </div>
                {!! Form::close() !!}
              </div>
            </div>
            

          </div>

        </div>
      </section><!-- #contact -->

    </main>
  @endsection

 