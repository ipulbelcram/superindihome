<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=m, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table class="table-borderless">
        <tr>
            <td>Name</td>
            <td class="px-3">:</td>
            <td>{{ $name }}</td>
        </tr>

        <tr>
            <td>Email</td>
            <td class="px-3">:</td>
            <td>{{ $email }}</td>
        </tr>

        <tr>
            <td>Message</td>
            <td class="px-3">:</td>
            <td>{{ $pesan }}</td>
        </tr>

    </table>
</body>
</html>