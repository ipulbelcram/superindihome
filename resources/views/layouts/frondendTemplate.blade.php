<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Super Indihome</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ asset('frondend/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ asset('frondend/lib/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{ asset('frondend/lib/animate/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frondend/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frondend/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frondend/lib/owlcarousel/assets/owl.theme.default.min.css') }}" rel="stylesheet">
  <link href="{{ asset('frondend/lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ asset('frondend/css/style.css') }}" rel="stylesheet">
  @yield('style')
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header" class="fixed-top">
    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <h1 class="text-light"><a href="#header"><span>NewBiz</span></a></h1> -->
        <a href="{{ route('home') }}" class="scrollto"><img src="{{ asset('image/logo-indihome-jabodetabek.png') }}" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="#slider">Home</a></li>
          <li><a href="#about">Tentang Kami</a></li>
          <li><a href="#packages">Paket</a></li>
          <li><a href="#contact">Kontak Kami</a></li>
        </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  @yield('content')

   <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-6 footer-info">
            <!-- <h3>INDIHOME</h3> -->
            <img src="{{ asset('image/logo-indihome-jabodetabek.png') }}" class ="img-fluid  mb-3">
            <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus. Scelerisque felis imperdiet proin fermentum leo. Amet volutpat consequat mauris nunc congue.</p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">Tentang Kami</a></li>
              <li><a href="#">Paket</a></li>
              <li><a href="#">Kontak Kami</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Kontak Kami</h4>
            <p>
              {{ $address }} <br>
              <strong>Phone:</strong> {{ $contact }}<br>
              <strong>Email:</strong> {{ $email }}<br>
            </p>

            <!-- <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div> -->

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Our Newsletter</h4>
            <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna veniam enim veniam illum dolore legam minim quorum culpa amet magna export quem marada parida nodela caramase seza.</p>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; {{ date('Y') }} Copyright <strong>superindihome.com</strong>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <a href="https://api.whatsapp.com/send?phone=62089657341120" class="" style="position:fixed; bottom:60px; right:0;"><img src="{{ asset('image/icon/whatsapp.png') }}" class="img-fluid"></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <!-- JavaScript Libraries -->
  <script src="{{ asset('frondend/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/mobile-nav/mobile-nav.js') }}"></script>
  <script src="{{ asset('frondend/lib/wow/wow.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/waypoints/waypoints.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/isotope/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('frondend/lib/lightbox/js/lightbox.min.js') }}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('frondend/lib/contactform/contactform.js') }}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('frondend/js/main.js') }}"></script>
  <script src="{{ asset('js/slider.js') }}"></script>
  @yield('script')
</body>
</html>
