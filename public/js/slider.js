
$(document).ready(function() {
    var owl = $('.owl-carousel')
    owl.owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        responsiveClass: true,
        responsive: {
            0: {
                items:1,
                stagePadding: 15,
                margin: 15
            },
            540: {
                items:1,
                stagePadding: 15,
                margin: 15
            },
            720: {
                items:1,
                stagePadding: 70,
                margin: 35,
                nav: true,
                navText:
                [
                    "<img src='public/image/icon/arrow-left.png' title='Previous Slide'>",
                    "<img src='public/image/icon/arrow-right.png' title='Next Slide'>"
                ]
            },
            960: {
                items:1,
                stagePadding: 70,
                margin: 35,
                nav: true,
                navText:
                [
                    "<img src='public/image/icon/arrow-left.png' title='Previous Slide'>",
                    "<img src='public/image/icon/arrow-right.png' title='Next Slide'>"
                ]
            },
            1140: {
                items:1,
                stagePadding: 100,
                margin: 20,
                nav: true,
                navText:
                [
                    "<img src='public/image/icon/arrow-left.png' title='Previous Slide'>",
                    "<img src='public/image/icon/arrow-right.png' title='Next Slide'>"
                ]
            }
        }
    })
})
        