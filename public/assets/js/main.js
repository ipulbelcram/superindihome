$(document).ready(function() {
	$('#menu').click(function() {
		if(!$('.sidebar').hasClass('show')) {
			$('.sidebar').addClass('show')
			$('.sidebar').css('left','0px')
			$('.overlay').show()
		} else {
			$('.sidebar').removeClass('show')
			$('.sidebar').css('left','-230px')
			$('.overlay').hide()
		}
	})
	$('.overlay').click(function() {
		$('.sidebar').removeClass('show')
		$('.sidebar').css('left','-230px')
		$(this).hide()
	})
	$(document.body).click(function() {
		$('.alert').animate({bottom:"+=100px"},150)
		setTimeout(function(){
			$('.alert').animate({bottom:"-=100px"},150)
		},3000)
	})
	$('.password').click(function(){
		if($(this).hasClass('mdi-eye')){
			$(this).removeClass('mdi-eye')
			$(this).addClass('mdi-eye-off')
			if($(this).data('id') == 'password'){
				$('#password').attr('type','password')
			} else if($(this).data('id') == 'npassword'){
				$('#npassword').attr('type','password')
			} else {
				$('#cpassword').attr('type','password')
			}
		} else {
			$(this).addClass('mdi-eye')
			$(this).removeClass('mdi-eye-off')
			if($(this).data('id') == 'password'){
				$('#password').attr('type','text')
			} else if($(this).data('id') == 'npassword'){
				$('#npassword').attr('type','text')
			} else {
				$('#cpassword').attr('type','text')
			}
		}
	})
	$('#signout').click(function(){
		$.ajax({
			url: '/session/signout',
			type: 'GET',
			success: function(){
				window.location.href = '/signin'
			}
		})
	})
})