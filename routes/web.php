<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController')->name('home');
Route::post('/sendMail', 'EmailController@sendEmail')->name('sendMail');

Auth::routes(['register' => false]);

Route::get('/dashboard', function() {
    return view('admin/index');
})->name('dashboard')->middleware('auth');

Route::namespace('Admin')->group(function () {
    Route::resource('packages', 'PackageController');
    Route::resource('sliders', 'SliderController');
    Route::resource('web_setting', 'WebSettingController')->only([
        'index', 'store'
    ]);
    Route::resource('forms', 'FormsController')->only([
        'index', 'store', 'show', 'destroy'
    ]);
});
Route::get('/pendaftaran_alert', function() {
    return view('partials/formAlert');
});

Route::get('/contact_alert', function() {
    return view('partials/contactAlert');
});
