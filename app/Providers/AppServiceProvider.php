<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\Models\Param;
use Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        $halaman = "";
        if(Request::segment(1) == "dashboard") {
            $halaman = "dashboard";
        }

        if(Request::segment(1) == "web_setting") {
            $halaman = "web_setting";
        }

        if(Request::segment(1) == "sliders") {
            $halaman = "sliders";
        }

        if(Request::segment(1) == "packages") {
            $halaman = "packages";
        }

        if(Request::segment(1) == "forms") {
            $halaman = "forms";
        }


        $contact = Param::select('param')->where('category_param', 'contact')->first();
        $address = Param::select('param')->where('category_param', 'address')->first();
        $email = 'ipulbelcram@gmail.com';
        View::share([
            'halaman' => $halaman,
            'contact' => $contact->param,
            'address' => $address->param,
            'email' => $email,
        ]);
        Schema::defaultStringLength(191);
    }
}
