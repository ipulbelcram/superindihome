<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = 'packages';

    protected $fillable = [
        'image', 'title', 'package_name', 'slug', 'content', 'active'
    ];
    
    protected $primaryKey = 'slug';

    public $incrementing = false;

    public function forms()
    {
        return $this->hasMany('App\Models\Form', 'package_id');
    }

}
