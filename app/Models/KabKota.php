<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KabKota extends Model
{
    protected $table = 'kab_kota';

    protected $fillable = [
        'provinsi_id','kab_kot','type','ibu_kota'
    ];

    public $timestamps = false;
}
