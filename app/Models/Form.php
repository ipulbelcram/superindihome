<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = 'forms';

    protected $fillable = [
        'package_id','nama','email','no_hp','provinsi_id','kab_kota_id','alamat_pemasangan'
    ];

    public function packages()
    {
        return $this->belongsTo('App\Models\Package', 'package_id', 'id');
    }

    public function provinsi()
    {
        return $this->belongsTo('App\Models\Provinsi', 'provinsi_id');
    }

    public function kab_kota()
    {
        return $this->belongsTo('App\Models\KabKota', 'kab_kota_id');
    }
}
