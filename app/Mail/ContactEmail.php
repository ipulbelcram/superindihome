<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $name;
    public $email;
    public $message;

    public function __construct($request)
    {
        $this->name = $request->input('name');
        $this->email = $request->input('email');
        $this->message = $request->input('message');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('superIndihome@superIndihome.com')
        ->view('contactEmail')
        ->with(
         [
             'name' => $this->name,
             'email' => $this->email,
             'pesan'=> $this->message,
         ]);
    }
}
