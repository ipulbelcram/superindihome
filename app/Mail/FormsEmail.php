<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FormsEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $package_name;
    protected $name;
    protected $emaill;
    protected $no_hp;
    protected $alamat_pemasangan;

    public function __construct($request)
    {
        $this->package_name = $request->input('package_name');
        $this->name = $request->input('nama');
        $this->email = $request->input('email');
        $this->no_hp = $request->input('no_hp');
        $this->alamat_pemasangan = $request->input('alamat_pemasangan');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return 
        $this->from('superIndihome@superIndihome.com')
        ->view('formEmail')
        ->with(
         [
             'package_name' => $this->package_name,
             'name' => $this->name,
             'email' => $this->email,
             'no_hp'=> $this->no_hp,
             'alamat_pemasangan' => $this->alamat_pemasangan,
         ]);
    }
}
