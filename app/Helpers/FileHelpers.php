<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Storage;

class FileHelpers {
    public static function imgBase64($file, $path)
    {
        $data = str_replace(' ', '+', $file);
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $file_name = Str::random(20).".png";
        $path = public_path() . $path . "/" . $file_name;
        file_put_contents($path, $data);
        
        return $file_name;
    }
    public static function cropImage($user_id, $profile_picture)
    {
        if(!empty($user_id) && !empty($profile_picture)) {
            $data = $profile_picture;
            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);
            $image_name = Str::random(20).".png";
            $path = public_path() . "/img/profile/" . $image_name;
            file_put_contents($path, $data);
            return $image_name;
        }
    }

    public static function uploadFile($path, $requestFile) {
        $image = $requestFile;
        $ext = $image->getClientOriginalExtension();

        if($requestFile->isValid()) {
            $file_name = Str::random(20).".$ext";
            $upload_path = $path;
            $requestFile->move($upload_path, $file_name);
            return $file_name;
        }
        return false;
    }

    public static function deleteFile($path, $data) 
    {
        $exist = Storage::disk('local_public')->exists($path.'/'.$data);
        if(isset($data)) {
            Storage::disk('local_public')->delete($path.'/'.$data);
        }
    }
}