<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH'){
            $image = ['sometimes', 'image', 'max:500', 'mimes:jpeg,jpg,png'];
        } else {
            $image = ['required', 'image', 'max:500', 'mimes:jpeg,jpg,png'];
        }

        return [
            'image' => $image,
            'title' => ['required', 'string'],
            'package_name' => ['required', 'string'],
            'content' => ['required', 'string'],
            'active' => ['required', 'in:true,false'],
        ];
    }
}
