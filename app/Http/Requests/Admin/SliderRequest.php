<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->method() == 'PATCH'){
            $image = ['sometimes', 'image', 'max:500', 'mimes:jpeg,jpg,png'];
            $order = ['required', 'numeric', 'max:10', 'unique:sliders,order,'.$this->slider];
        } else {
            $image = ['required', 'image', 'max:500', 'mimes:jpeg,jpg,png'];
            $order = ['required', 'numeric', 'max:10', 'unique:sliders,order'];
        }
        
        return [
            'image' => $image,
            'title' => ['required', 'string'],
            'order' => $order,
        ];
    }
}
