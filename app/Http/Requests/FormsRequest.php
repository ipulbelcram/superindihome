<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'package_id' => ['required', 'numeric', 'exists:packages,id'],
            'nama' =>  ['required', 'string'],
            'email' => ['required', 'email'],
            'no_hp' => ['required', 'numeric', 'digits_between:8,14'], 
            'provinsi_id' => ['required', 'numeric', 'exists:provinsi,id'],
            'kab_kota_id' => ['required', 'numeric', 'exists:kab_kota,id'],
            'alamat_pemasangan' => ['required', 'string']
        ];
    }
}
