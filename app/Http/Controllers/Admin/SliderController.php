<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\Http\Requests\Admin\SliderRequest;
use App\Services\Slider\SliderServices;
use Session;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 
            ['show']
        ]);
    }

    public function index()
    {
        $data_slider = SliderServices::getAll();
        return view('admin/slider/index', compact('data_slider'));
    }


    public function create()
    {
        return view('admin/slider/create');
    }

    public function store(SliderRequest $request)
    {
        SliderServices::create($request);
        Session::flash('flash_message', 'Data Slider Berhasil di Tambahkan');
        return redirect('sliders');
    }


    public function show($id)
    {
        //
    }


    public function edit(Slider $slider)
    {
        return view('admin/slider/edit', compact('slider'));
    }


    public function update(SliderRequest $request, $id)
    {
        SliderServices::update($request, $id);
        Session::flash('flash_message', 'Data Berhasil di Simpan');
        return redirect('sliders');
    }

    public function destroy(Request $request, $id)
    {
        SliderServices::delete($request);
        Session::flash('flash_message', 'Data Berhasil di Hapus');
        return redirect('sliders');
    }
}
