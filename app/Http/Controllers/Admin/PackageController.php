<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Package;
use App\Models\Provinsi;
use App\Models\KabKota;
use App\Services\Package\PackageServices;
use App\Http\Requests\Admin\PackageRequest;
use Session;

class PackageController extends Controller
{
    public function __construct() {
        $this->middleware('auth', ['except' => 
            ['show']
        ]);
    }

    public function index()
    {
        $data_package = PackageServices::getAll();
        return view('admin/package/index', compact('data_package'));
    }

    public function create()
    {
        return view('admin/package/create');
    }

    public function store(PackageRequest $request)
    {
        PackageServices::create($request);
        Session::flash('flash_message', 'Data Package Berhasil di Tambahkan');
        return redirect('packages');
        
    }

    public function show(Package $package)
    {
        return view('admin/package/show', compact('package'));
    }

    public function edit($slug)
    {

        $package = PackageServices::get_by_slug($slug);
        return view('admin/package/edit', compact('package'));
    }

    public function update(PackageRequest $request, $slug)
    {
        PackageServices::update($request, $slug);
        Session::flash('flash_message', 'Data Package Berhasil di Simpan');
        return redirect('packages');
    }

    public function destroy(Request $request, $id)
    {
        $id = $request->input('id');
        PackageServices::destroy($id);
        Session::flash('flash_message', 'Data Package Berhasil di Hapus');
        return redirect('packages');
    }
}
