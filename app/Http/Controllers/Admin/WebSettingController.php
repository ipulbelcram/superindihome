<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\WebSettingRequest;
use App\Models\Param;
use Session;

class WebSettingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $address = Param::where('category_param','address')->first();
        $contact = Param::where('category_param','contact')->first();
        $email = Param::where('category_param','email')->first();
        return view('admin/web_setting/index', compact('address', 'contact', 'email'));
    }

    
    public function create()
    {
        //
    }

    
    public function store(WebSettingRequest $request)
    {
        $where_address = Param::where('category_param','address');
        $where_contact = Param::where('category_param','contact');
        $where_email = Param::where('category_param','email');

        if($where_contact->count() < 1) {
            $inputC['category_param'] = 'contact';
            $inputC['param'] = $request->post('contact');
            $inputC['active'] = 'true';
            Param::create($inputC);
        } else {
            $inputC['param'] = $request->post('contact');
            $where_contact->update($inputC);
        }

        if($where_address->count() < 1) {
            $inputA['category_param'] = 'address';
            $inputA['param'] = $request->post('address');
            $inputA['active'] = 'true';
            Param::create($inputA);
        } else {
            $inputA['param'] = $request->post('address');
            $where_address->update($inputA);
        }

        if($where_email->count() < 1) {
            $inputA['category_param'] = 'email';
            $inputA['param'] = $request->post('email');
            $inputA['active'] = 'true';
            Param::create($inputA);
        } else {
            $inputA['param'] = $request->post('email');
            $where_email->update($inputA);
        }

        Session::flash('flash_message', 'Data Berhasil di Simpan');
        return redirect('web_setting');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
