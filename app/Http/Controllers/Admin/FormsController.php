<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\FormsRequest;
use App\Services\Form\FormsServices;
use App\Models\Form;
use Session;

class FormsController extends Controller
{
    public function index()
    {
        $data_forms = FormsServices::get();
        return view('admin/form/index', compact('data_forms'));
    }


    public function create()
    {
        //
    }

    public function store(FormsRequest $request)
    {
        FormsServices::create($request);
        return redirect('pendaftaran_alert');
    }

    public function show(Form $form)
    {
        return view('admin/form/show', compact('form'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Request $request, $id)
    {
        FormsServices::delete($request->post('id'));
        Session::flash('flash_message', 'Data berhasil di hapus');
        return redirect('forms');
    }
}
