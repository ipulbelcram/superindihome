<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Provinsi;
use App\Models\KabKota;
use App\Http\Resources\ProvinsiResource;
use App\Http\Resources\KabKotaResource;

class getController extends Controller
{
    public function getProvinsi()
    {
        $provinsi = Provinsi::all();
        return ProvinsiResource::collection($provinsi);
    }

    public function getKabKota($provinsi_id)
    {
        $kabKota = KabKota::where('provinsi_id', $provinsi_id)->get();
        return KabKotaResource::collection($kabKota);
    }
}
