<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\http\Requests\ContactRequests;
use App\Mail\ContactEmail;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendEmail(ContactRequests $request)
    {
        Mail::to("ipulbelcram@gmail.com")->send(new ContactEmail($request));
        return redirect('contact_alert');
    }
}
