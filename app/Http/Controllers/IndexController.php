<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Slider\SliderServices;
use App\Services\Package\PackageServices;

class IndexController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data_slider = SliderServices::getAll();
        $data_package = PackageServices::getAll(['active' => 'true']);
        return view('index', compact('data_slider', 'data_package'));
    }
}
