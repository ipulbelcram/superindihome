<?php
namespace App\Services\Package;

use FileHelper;
use App\Models\Package;
use Illuminate\Support\Str;

class PackageServices 
{
    public static function create($request)
    {
        $hitung = Package::where('title', $request->input('title'))->count();
        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = FileHelper::uploadFile('image/packages', $request->file('image'));
            $input['image'] = $image;
        }

        if($hitung > 0) {
            $total = $hitung+1;
            $input['slug'] = Str::slug($request->input('title'), '-').'-'.$total;
        } else {
            $input['slug'] = Str::slug($request->input('title'), '-');
        }

        Package::create($input);
    }

    public static function get_by_slug($slug)
    {
        $package = Package::findOrFail($slug);
        return $package;
    }

    public static function update($request, $slug)
    {
        $package = Package::findOrFail($slug);
        $hitung = Package::where('title', $request->input('title'))->count();
        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = FileHelper::uploadFile('image/packages', $request->file('image'));
            $input['image'] = $image;
            FileHelper::deleteFile('image/packages', $package->image);
        }

        if($hitung > 0) {
            $total = $hitung+1;
            $input['slug'] = Str::slug($request->input('title'), '-').'-'.$total;
        } else {
            $input['slug'] = Str::slug($request->input('title'), '-');
        }

        $package->update($input);
    }

    public static function destroy($id)
    {
        $package = Package::where('id', $id)->firstOrFail();
        FileHelper::deleteFile('image/packages', $package->image);
        $package->delete();
    }

    public static function getAll($where = null)
    {
        $package = Package::orderBy('id', 'DESC');
        if(!empty($where)) {
            $package->where($where);
        }
        $getPackages = $package->get();
        return $getPackages;
    }
}