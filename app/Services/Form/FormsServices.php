<?php

namespace App\Services\Form;

use App\Models\Form;
use App\Mail\FormsEmail;
use Illuminate\Support\Facades\Mail;

class FormsServices 
{
    public static function create($request)
    {
        Mail::to("ipulbelcram@gmail.com")->send(new FormsEmail($request));
        $input = $request->all();
        Form::create($input);
    }

    public static function get($where = null)
    {
        if(empty($where)) {
            return Form::orderBy('id', 'DESC')->paginate(10);
        } else {
            return Form::where($where)->first();
        }
    }

    public static function delete($id)
    {
        $form = Form::findOrFail($id);
        $form->delete();
    }
}