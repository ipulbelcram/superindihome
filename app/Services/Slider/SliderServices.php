<?php

namespace App\Services\Slider;

use FileHelper;
use App\Models\Slider;

class SliderServices 
{
    public static function create($request)
    {
        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = FileHelper::uploadFile('image/sliders', $request->file('image'));
            $input['image'] = $image;
        }

        Slider::create($input);
    }

    public static function update($request, $id)
    {
        $slider = Slider::findOrFail($id);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $image = FileHelper::uploadFile('image/sliders', $request->file('image'));
            $input['image'] = $image;
            FileHelper::deleteFile('image/sliders', $slider->image);
        }

        $slider->update($input);
    }

    public static function delete($request)
    {
        $slider = Slider::findOrFail($request->input('id'));
        FileHelper::deleteFile('image/sliders', $slider->image);
        $slider->delete();
    }

    public static function getAll()
    {
        $slider = Slider::orderBy('order', 'ASC')->get();
        return $slider;
    }
}