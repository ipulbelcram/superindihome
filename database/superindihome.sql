-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Jul 2020 pada 20.41
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `superindihome`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `forms`
--

CREATE TABLE `forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `package_id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi_id` bigint(20) UNSIGNED NOT NULL,
  `kab_kota_id` bigint(20) UNSIGNED NOT NULL,
  `alamat_pemasangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `forms`
--

INSERT INTO `forms` (`id`, `package_id`, `nama`, `email`, `no_hp`, `provinsi_id`, `kab_kota_id`, `alamat_pemasangan`, `created_at`, `updated_at`) VALUES
(5, 1, 'Ahmad Syaiful Akbar', 'ipulbelcram@gmail.com', '089657341120', 52, 5201, 'asd', '2020-07-22 11:30:26', '2020-07-22 11:30:26'),
(6, 1, 'Ahmad Syaiful Akbar', 'ipulbelcram@gmail.com', '089657341120', 51, 5101, 'asd', '2020-07-22 11:32:04', '2020-07-22 11:32:04'),
(7, 1, 'Ahmad Syaiful Akbar', 'admin@intrex.id', '089657341120', 53, 5301, 'asd', '2020-07-22 11:38:14', '2020-07-22 11:38:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kab_kota`
--

CREATE TABLE `kab_kota` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provinsi_id` bigint(20) UNSIGNED NOT NULL,
  `kab_kota` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `ibu_kota` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kab_kota`
--

INSERT INTO `kab_kota` (`id`, `provinsi_id`, `kab_kota`, `type`, `ibu_kota`) VALUES
(1101, 11, 'Kab. Simeulue', 2, 'Sinabang'),
(1102, 11, 'Kab. Aceh Singkil', 2, 'Singkil'),
(1103, 11, 'Kab. Aceh Selatan', 2, 'Tapak Tuan'),
(1104, 11, 'Kab. Aceh Tenggara', 2, 'Kutacane'),
(1105, 11, 'Kab. Aceh Timur', 2, 'Langsa'),
(1106, 11, 'Kab. Aceh Tengah', 2, 'Takengon'),
(1107, 11, 'Kab. Aceh Barat', 2, 'Meulaboh'),
(1108, 11, 'Kab. Aceh Besar', 2, 'Jantho'),
(1109, 11, 'Kab. Pidie', 2, 'Sigli'),
(1110, 11, 'Kab. Bireuen', 2, 'Bireun'),
(1111, 11, 'Kab. Aceh Utara', 2, 'Lhoksukon'),
(1112, 11, 'Kab. Aceh Barat Daya', 2, 'Blangpidie'),
(1113, 11, 'Kab. Gayo Lues', 2, 'Blangkejeren'),
(1114, 11, 'Kab. Aceh Tamiang', 2, 'Karang Baru'),
(1115, 11, 'Kab. Nagan Raya', 2, 'Suka Makmue'),
(1116, 11, 'Kab. Aceh Jaya', 2, 'Calang'),
(1117, 11, 'Kab. Bener Meriah', 2, 'Simpang Tiga Rede'),
(1118, 11, 'Kab. Pidie Jaya', 2, 'Meureundu'),
(1171, 11, 'Kota Banda Aceh', 1, 'Banda Aceh'),
(1172, 11, 'Kota Sabang', 1, 'Sabang'),
(1173, 11, 'Kota Langsa', 1, 'Langsa'),
(1174, 11, 'Kota Lhokseumawe', 1, 'Lhokseumawe'),
(1175, 11, 'Kota Subulussalam', 1, 'Subulussalam'),
(1201, 12, 'Kab. Nias', 2, 'Gunung Sitoli'),
(1202, 12, 'Kab. Mandailing Natal', 2, 'Panyabungan'),
(1203, 12, 'Kab. Tapanuli Selatan', 2, 'Padang Sidempuan'),
(1204, 12, 'Kab. Tapanuli Tengah', 2, 'Sibolga'),
(1205, 12, 'Kab. Tapanuli Utara', 2, 'Tarutung'),
(1206, 12, 'Kab. Toba Samosir', 2, 'Balige'),
(1207, 12, 'Kab. Labuhan Batu', 2, 'Rantau Prapat'),
(1208, 12, 'Kab. Asahan', 2, 'Kisaran'),
(1209, 12, 'Kab. Simalungun', 2, 'Pematang Siantar'),
(1210, 12, 'Kab. Dairi', 2, 'Sidikalang'),
(1211, 12, 'Kab. Karo', 2, 'Kabanjahe'),
(1212, 12, 'Kab. Deli Serdang', 2, 'Lubuk Pakam'),
(1213, 12, 'Kab. Langkat', 2, 'Stabat'),
(1214, 12, 'Kab. Nias Selatan', 2, 'Teluk Dalam'),
(1215, 12, 'Kab. Humbang Hasundutan', 2, 'Dolok Sanggul'),
(1216, 12, 'Kab. Pak Pak Bharat', 2, 'Salak'),
(1217, 12, 'Kab. Samosir', 2, 'Pangururan'),
(1218, 12, 'Kab. Serdang Bedagai', 2, 'Sei Rampah'),
(1219, 12, 'Kab. Batu Bara', 2, 'Lima Puluh'),
(1220, 12, 'Kab. Padang Lawas Utara', 2, 'Gunung Tua'),
(1221, 12, 'Kab. Padang Lawas', 2, 'Sibuhuan'),
(1222, 12, 'Kab. Labuhanbatu Selatan', 2, 'Kotapinang'),
(1223, 12, 'Kab. Labuhanbatu Utara', 2, 'Aek Kanopan'),
(1224, 12, 'Kab. Nias Utara', 2, 'Lotu'),
(1225, 12, 'Kab. Nias Barat', 2, 'Lahomi'),
(1271, 12, 'Kota Sibolga', 1, 'Sibolga'),
(1272, 12, 'Kota Tanjung Balai', 1, 'Tanjung Balai'),
(1273, 12, 'Kota Pematang Siantar', 1, 'Pematang Siantar'),
(1274, 12, 'Kota Tebing Tinggi', 1, 'Kota Tebing Tinggi'),
(1275, 12, 'Kota Medan', 1, 'Medan'),
(1276, 12, 'Kota Binjai', 1, 'Binjai'),
(1277, 12, 'Kota Padang Sidempuan', 1, 'Padang Sidempuan'),
(1278, 12, 'Kota Gunung Sitoli', 1, 'Gunungsitoli'),
(1301, 13, 'Kab. Kepulauan Mentawai', 2, 'Tuapejat'),
(1302, 13, 'Kab. Pesisir Selatan', 2, 'Painan'),
(1303, 13, 'Kab. Solok', 2, 'Arosuka'),
(1304, 13, 'Kab. Sawahlunto/ Sijunjung', 2, ''),
(1305, 13, 'Kab. Tanah Datar', 2, 'Batusangkar'),
(1306, 13, 'Kab. Padang Pariaman', 2, 'Nagari Parit Malinta'),
(1307, 13, 'Kab. Agam', 2, 'Lubuk Basung'),
(1308, 13, 'Kab. Lima Puluh Kota', 2, 'Sarilamak'),
(1309, 13, 'Kab. Pasaman', 2, 'Lubuk Sikaping'),
(1310, 13, 'Kab. Solok Selatan', 2, 'Padang Aro'),
(1311, 13, 'Kab. Dharmas Raya', 2, 'Pulau Punjung'),
(1312, 13, 'Kab. Pasaman Barat', 2, 'Simpang Empat'),
(1371, 13, 'Kota Padang', 1, 'Kototangah'),
(1372, 13, 'Kota Solok', 1, 'Solok'),
(1373, 13, 'Kota Sawah Lunto', 1, 'Sawahlunto'),
(1374, 13, 'Kota Padang Panjang', 1, 'Padang Panjang'),
(1375, 13, 'Kota Bukittinggi', 1, 'Bukittinggi'),
(1376, 13, 'Kota Payakumbuh', 1, 'Payakumbuh'),
(1377, 13, 'Kota Pariaman', 1, 'Pariaman'),
(1401, 14, 'Kab. Kuantan Singingi', 2, 'Teluk Kuantan'),
(1402, 14, 'Kab. Indragiri Hulu', 2, 'Rengat'),
(1403, 14, 'Kab. Indragiri Hilir', 2, 'Tembilahan'),
(1404, 14, 'Kab. Pelalawan', 2, 'Pangkalan Kerinci'),
(1405, 14, 'Kab. Siak', 2, 'Siak Sriindrapura'),
(1406, 14, 'Kab. Kampar', 2, 'Bangkinang'),
(1407, 14, 'Kab. Rokan Hulu', 2, 'Pasir Pengarairan'),
(1408, 14, 'Kab. Bengkalis', 2, 'Bengkalis'),
(1409, 14, 'Kab. Rokan Hilir', 2, 'Bagansiapiapi'),
(1410, 14, 'Kab. Kep. Meranti', 2, 'Tebing Tinggi'),
(1471, 14, 'Kota Pekanbaru', 1, 'Pekanbaru'),
(1473, 14, 'Kota Dumai', 1, 'Dumai'),
(1501, 15, 'Kab. Kerinci', 2, 'Siulak'),
(1502, 15, 'Kab. Merangin', 2, 'Bangko'),
(1503, 15, 'Kab. Sarolangun', 2, 'Sarolangun'),
(1504, 15, 'Kab. Batang Hari', 2, 'Muara Bulian'),
(1505, 15, 'Kab. Muaro Jambi', 2, 'Sengeti'),
(1506, 15, 'Kab. Tanjung Jabung Timur', 2, 'Muara Sabak'),
(1507, 15, 'Kab. Tanjung Jabung Barat', 2, 'Kuala Tungkal'),
(1508, 15, 'Kab. Tebo', 2, 'Muara Tebo'),
(1509, 15, 'Kab. Bungo', 2, 'Muara Bungo'),
(1571, 15, 'Kota Jambi', 1, 'Jambi'),
(1572, 15, 'Kota. Sungai Penuh', 1, ''),
(1601, 16, 'Kab. Ogan Komering Ulu', 2, 'Baturaja'),
(1602, 16, 'Kab. Ogan Komering Ilir', 2, 'Kayu Agung'),
(1603, 16, 'Kab. Muara Enim', 2, 'Muara Enim'),
(1604, 16, 'Kab. Lahat', 2, 'Lahat'),
(1605, 16, 'Kab. Musi Rawas', 2, 'Lubuk Linggau'),
(1606, 16, 'Kab. Musi Banyu Asin', 2, 'Sekayu'),
(1607, 16, 'Kab. Banyuasin', 2, 'Pangkalan Balai'),
(1608, 16, 'Kab. Ogan Komering Ulu Sel.', 2, ''),
(1609, 16, 'Kab. Ogan Komering Ulu Timur', 2, 'Martapura'),
(1610, 16, 'Kab. Ogan Ilir', 2, 'Indralaya'),
(1611, 16, 'Kab. Empat Lawang', 2, 'Tebing Tinggi'),
(1612, 16, 'Kab. Penukal Abab', 2, ''),
(1671, 16, 'Kota Palembang', 1, 'Pelembang'),
(1672, 16, 'Kota Prabumulih', 1, 'Prabumulih'),
(1673, 16, 'Kota Pagar Alam', 1, 'Pagar Alam'),
(1674, 16, 'Kota Lubuk Linggau', 1, 'Lubuk Linggau'),
(1701, 17, 'Kab. Bengkulu Selatan', 2, 'Manna'),
(1702, 17, 'Kab. Rejang Lebong', 2, 'Curup'),
(1703, 17, 'Kab. Bengkulu Utara', 2, 'Arga Makmur'),
(1704, 17, 'Kab. Kaur', 2, 'Bintuhan'),
(1705, 17, 'Kab. Seluma', 2, 'Tais'),
(1706, 17, 'Kab. Muko-Muko', 2, 'Mukomuko'),
(1707, 17, 'Kab. Lebong', 2, 'Tubei'),
(1708, 17, 'Kab. Kepahiang', 2, 'Kepahiang'),
(1709, 17, 'Kab. Bengkulu Tengah', 2, 'Karang Tinggi'),
(1771, 17, 'Kota Bengkulu', 1, 'Bengkulu'),
(1801, 18, 'Kab. Lampung Barat', 2, 'Liwa'),
(1802, 18, 'Kab. Tanggamus', 2, 'Kota Agung'),
(1803, 18, 'Kab. Lampung Selatan', 2, 'Kalianda'),
(1804, 18, 'Kab. Lampung Timur', 2, 'Sukadana'),
(1805, 18, 'Kab. Lampung Tengah', 2, 'Gunung Sugih'),
(1806, 18, 'Kab. Lampung Utara', 2, 'Kotabumi'),
(1807, 18, 'Kab. Way Kanan', 2, 'Blambangan Umpu'),
(1808, 18, 'Kab. Tulang Bawang', 2, 'Menggala'),
(1809, 18, 'Kab. Pesawaran', 2, 'Gedong Tataan'),
(1810, 18, 'Kab. Pringsewu', 2, 'Pringsewu'),
(1811, 18, 'Kab. Mesuji', 2, 'Mesuji'),
(1812, 18, 'Kab. Tulang Bawang Barat', 2, 'Tulang Bawang Ten'),
(1813, 18, 'Kab. Pesisir Barat', 2, ''),
(1871, 18, 'Kota Bandar Lampung', 1, 'Bandar Lampung'),
(1872, 18, 'Kota Metro', 1, 'Metro'),
(1901, 19, 'Kab. Bangka', 2, 'Sungai Liat'),
(1902, 19, 'Kab. Belitung', 2, 'Tanjung Pandan'),
(1903, 19, 'Kab. Bangka Barat', 2, 'Mentok'),
(1904, 19, 'Kab. Bangka Tengah', 2, 'Koba'),
(1905, 19, 'Kab. Bangka Selatan', 2, 'Toboali'),
(1906, 19, 'Kab. Belitung Timur', 2, 'Manggar'),
(1971, 19, 'Kota Pangkalpinang', 1, 'Pangkal Pinang'),
(2101, 21, 'Kab. Karimun', 2, 'Tj. Balai Karimun'),
(2102, 21, 'Kab. Bintan', 2, 'Teluk Bintan, Bintan'),
(2103, 21, 'Kab. Natuna', 2, 'Ranai'),
(2104, 21, 'Kab. Lingga', 2, 'Daik Lingga'),
(2105, 21, 'Kab. Kep. Anambas', 2, 'Tarempa'),
(2171, 21, 'Kota Batam', 1, 'Batam'),
(2172, 21, 'Kota Tanjungpinang', 1, 'Tanjung Pinang'),
(3101, 31, 'Kab. Kepulauan Seribu', 2, 'P. Seribu'),
(3171, 31, 'Kota Jakarta Selatan', 1, 'Kebayoran Baru'),
(3172, 31, 'Kota Jakarta Timur', 1, 'Cakung'),
(3173, 31, 'Kota Jakarta Pusat', 1, 'Tanah Abang'),
(3174, 31, 'Kota Jakarta Barat', 1, 'Grogol Petamburan'),
(3175, 31, 'Kota Jakarta Utara', 1, 'Tanjung Priok'),
(3201, 32, 'Kab. Bogor', 2, 'Cibinong'),
(3202, 32, 'Kab. Sukabumi', 2, 'Sukabumi'),
(3203, 32, 'Kab. Cianjur', 2, 'Cianjur'),
(3204, 32, 'Kab. Bandung', 2, 'Soreang'),
(3205, 32, 'Kab. Garut', 2, 'Garut'),
(3206, 32, 'Kab. Tasikmalaya', 2, 'Singaparna'),
(3207, 32, 'Kab. Ciamis', 2, 'Ciamis'),
(3208, 32, 'Kab. Kuningan', 2, 'Kuningan'),
(3209, 32, 'Kab. Cirebon', 2, 'Sumber'),
(3210, 32, 'Kab. Majalengka', 2, 'Majalengka'),
(3211, 32, 'Kab. Sumedang', 2, 'Sumedang'),
(3212, 32, 'Kab. Indramayu', 2, 'Indramayu'),
(3213, 32, 'Kab. Subang', 2, 'Subang'),
(3214, 32, 'Kab. Purwakarta', 2, 'Purwakarta'),
(3215, 32, 'Kab. Karawang', 2, 'Karawang'),
(3216, 32, 'Kab. Bekasi', 2, 'Cikarang'),
(3217, 32, 'Kab. Bandung Barat', 2, 'Ngamprah'),
(3218, 32, 'Kab. Pangandaran', 2, 'Pangandaran'),
(3271, 32, 'Kota Bogor', 1, 'Bogor'),
(3272, 32, 'Kota Sukabumi', 1, 'Sukabumi'),
(3273, 32, 'Kota Bandung', 1, 'Bandung'),
(3274, 32, 'Kota Cirebon', 1, 'Cirebon'),
(3275, 32, 'Kota Bekasi', 1, 'Bekasi'),
(3276, 32, 'Kota Depok', 1, 'Depok'),
(3277, 32, 'Kota Cimahi', 1, 'Cimahi'),
(3278, 32, 'Kota Tasikmalaya', 1, 'Tasikmalaya'),
(3279, 32, 'Kota Banjar', 1, 'Banjar'),
(3301, 33, 'Kab. Cilacap', 2, 'Cilacap'),
(3302, 33, 'Kab. Banyumas', 2, 'Purwokerto'),
(3303, 33, 'Kab. Purbalingga', 2, 'Purbalingga'),
(3304, 33, 'Kab. Banjarnegara', 2, 'Banjarnegara'),
(3305, 33, 'Kab. Kebumen', 2, 'Kebumen'),
(3306, 33, 'Kab. Purworejo', 2, 'Purworejo'),
(3307, 33, 'Kab. Wonosobo', 2, 'Wonosobo'),
(3308, 33, 'Kab. Magelang', 2, 'Mungkid'),
(3309, 33, 'Kab. Boyolali', 2, 'Boyolali'),
(3310, 33, 'Kab. Klaten', 2, 'Klaten'),
(3311, 33, 'Kab. Sukoharjo', 2, 'Sukoharjo'),
(3312, 33, 'Kab. Wonogiri', 2, 'Wonogiri'),
(3313, 33, 'Kab. Karanganyar', 2, 'Karanganyar'),
(3314, 33, 'Kab. Sragen', 2, 'Sragen'),
(3315, 33, 'Kab. Grobogan', 2, 'Purwodadi'),
(3316, 33, 'Kab. Blora', 2, 'Blora'),
(3317, 33, 'Kab. Rembang', 2, 'Rembang'),
(3318, 33, 'Kab. Pati', 2, 'Pati'),
(3319, 33, 'Kab. Kudus', 2, 'Kudus'),
(3320, 33, 'Kab. Jepara', 2, 'Jepara'),
(3321, 33, 'Kab. Demak', 2, 'Demak'),
(3322, 33, 'Kab. Semarang', 2, 'Ungaran'),
(3323, 33, 'Kab. Temanggung', 2, 'Temanggung'),
(3324, 33, 'Kab. Kendal', 2, 'Kendal'),
(3325, 33, 'Kab. Batang', 2, 'Batang'),
(3326, 33, 'Kab. Pekalongan', 2, 'Kajen'),
(3327, 33, 'Kab. Pemalang', 2, 'Pemalang'),
(3328, 33, 'Kab. Tegal', 2, 'Slawi'),
(3329, 33, 'Kab. Brebes', 2, 'Brebes'),
(3371, 33, 'Kota Magelang', 1, 'Magelang'),
(3372, 33, 'Kota Surakarta', 1, 'Surakarta'),
(3373, 33, 'Kota Salatiga', 1, 'Salatiga'),
(3374, 33, 'Kota Semarang', 1, 'Semarang'),
(3375, 33, 'Kota Pekalongan', 1, 'Pekalongan'),
(3376, 33, 'Kota Tegal', 1, 'Tegal'),
(3401, 34, 'Kab. Kulon Progo', 2, 'Wates'),
(3402, 34, 'Kab. Bantul', 2, 'Bantul'),
(3403, 34, 'Kab. Gunung Kidul', 2, 'Wonosari'),
(3404, 34, 'Kab. Sleman', 2, 'Sleman'),
(3471, 34, 'Kota Yogyakarta', 1, 'Yogyakarta'),
(3501, 35, 'Kab. Pacitan', 2, 'Pacitan'),
(3502, 35, 'Kab. Ponorogo', 2, 'Ponorogo'),
(3503, 35, 'Kab. Trenggalek', 2, 'Trenggalek'),
(3504, 35, 'Kab. Tulungagung', 2, 'Tulungagung'),
(3505, 35, 'Kab. Blitar', 2, 'Kanigoro'),
(3506, 35, 'Kab. Kediri', 2, 'Kediri'),
(3507, 35, 'Kab. Malang', 2, 'Kepanjen'),
(3508, 35, 'Kab. Lumajang', 2, 'Lumajang'),
(3509, 35, 'Kab. Jember', 2, 'Jember'),
(3510, 35, 'Kab. Banyuwangi', 2, 'Banyuwangi'),
(3511, 35, 'Kab. Bondowoso', 2, 'Bondowoso'),
(3512, 35, 'Kab. Situbondo', 2, 'Situbondo'),
(3513, 35, 'Kab. Probolinggo', 2, 'Kraksaan'),
(3514, 35, 'Kab. Pasuruan', 2, 'Pasuruan'),
(3515, 35, 'Kab. Sidoarjo', 2, 'Sidoarjo'),
(3516, 35, 'Kab. Mojokerto', 2, 'Mojokerto'),
(3517, 35, 'Kab. Jombang', 2, 'Jombang'),
(3518, 35, 'Kab. Nganjuk', 2, 'Nganjuk'),
(3519, 35, 'Kab. Madiun', 2, 'Mejayan'),
(3520, 35, 'Kab. Magetan', 2, 'Magetan'),
(3521, 35, 'Kab. Ngawi', 2, 'Ngawi'),
(3522, 35, 'Kab. Bojonegoro', 2, 'Bojonegoro'),
(3523, 35, 'Kab. Tuban', 2, 'Tuban'),
(3524, 35, 'Kab. Lamongan', 2, 'Lamongan'),
(3525, 35, 'Kab. Gresik', 2, 'Gresik'),
(3526, 35, 'Kab. Bangkalan', 2, 'Bangkalan'),
(3527, 35, 'Kab. Sampang', 2, 'Sampang'),
(3528, 35, 'Kab. Pamekasan', 2, 'Pamekasan'),
(3529, 35, 'Kab. Sumenep', 2, 'Sumenep'),
(3571, 35, 'Kota Kediri', 1, 'Kediri'),
(3572, 35, 'Kota Blitar', 1, 'Blitar'),
(3573, 35, 'Kota Malang', 1, 'Malang'),
(3574, 35, 'Kota Probolinggo', 1, 'Probolinggo'),
(3575, 35, 'Kota Pasuruan', 1, 'Pasuruan'),
(3576, 35, 'Kota Mojokerto', 1, 'Mojokerto'),
(3577, 35, 'Kota Madiun', 1, 'Madiun'),
(3578, 35, 'Kota Surabaya', 1, 'Surabaya'),
(3579, 35, 'Kota Batu', 1, 'Batu'),
(3601, 36, 'Kab. Pandeglang', 2, 'Pandeglang'),
(3602, 36, 'Kab. Lebak', 2, 'Rangkas Bitung'),
(3603, 36, 'Kab. Tangerang', 2, 'Tigaraksa'),
(3604, 36, 'Kab. Serang', 2, 'Serang'),
(3671, 36, 'Kota Tangerang', 1, 'Tangerang'),
(3672, 36, 'Kota Cilegon', 1, 'Cilegon'),
(3673, 36, 'Kota Serang', 1, 'Serang'),
(3674, 36, 'Kota Tangerang Selatan', 1, 'Tangerang'),
(5101, 51, 'Kab. Jembrana', 2, 'Negara'),
(5102, 51, 'Kab. Tabanan', 2, 'Tabanan'),
(5103, 51, 'Kab. Badung', 2, 'Mengwi'),
(5104, 51, 'Kab. Gianyar', 2, 'Gianyar'),
(5105, 51, 'Kab. Klungkung', 2, 'Semarapura'),
(5106, 51, 'Kab. Bangli', 2, 'Bangli'),
(5107, 51, 'Kab. Karang Asem', 2, 'Amlapura'),
(5108, 51, 'Kab. Buleleng', 2, 'Singaraja'),
(5171, 51, 'Kota Denpasar', 1, 'Denpasar'),
(5201, 52, 'Kab. Lombok Barat', 2, 'Mataram'),
(5202, 52, 'Kab. Lombok Tengah', 2, 'Praya'),
(5203, 52, 'Kab. Lombok Timur', 2, 'Selong'),
(5204, 52, 'Kab. Sumbawa', 2, 'Sumbawa Besar'),
(5205, 52, 'Kab. Dompu', 2, 'Dompu'),
(5206, 52, 'Kab. Bima', 2, 'Woha'),
(5207, 52, 'Kab. Sumbawa Barat', 2, 'Taliwang'),
(5208, 52, 'Kab. Lombok Utara', 2, 'Tanjung'),
(5271, 52, 'Kota Mataram', 1, 'Mataram'),
(5272, 52, 'Kota Bima', 1, 'Bima'),
(5301, 53, 'Kab. Sumba Barat', 2, 'Waikabubak'),
(5302, 53, 'Kab. Sumba Timur', 2, 'Waingapu'),
(5303, 53, 'Kab. Kupang', 2, 'Kupang'),
(5304, 53, 'Kab. Timor Tengah Selatan', 2, 'Soe'),
(5305, 53, 'Kab. Timor Tengah Utara', 2, 'Kafemananu'),
(5306, 53, 'Kab. Belu', 2, 'Atambua'),
(5307, 53, 'Kab. Alor', 2, 'Kalabahi'),
(5308, 53, 'Kab. Lembata', 2, 'Lewoleba'),
(5309, 53, 'Kab. Flores Timur', 2, 'Larantuka'),
(5310, 53, 'Kab. Sikka', 2, 'Maumere'),
(5311, 53, 'Kab. Ende', 2, 'Ende'),
(5312, 53, 'Kab. Ngada', 2, 'Bajawa'),
(5313, 53, 'Kab. Manggarai', 2, 'Ruteng'),
(5314, 53, 'Kab. Rote-Ndao', 2, 'Baa'),
(5315, 53, 'Kab. Manggarai Barat', 2, 'Labuan Bajo'),
(5316, 53, 'Kab. Sumba Tengah', 2, 'Waibakul'),
(5317, 53, 'Kab. Sumba Barat Daya', 2, 'Tambolaka'),
(5318, 53, 'Kab. Nagekeo', 2, 'Mbay'),
(5319, 53, 'Kab. Manggarai Timur', 2, 'Borong'),
(5320, 53, 'Kab. Sabu Raijua', 2, 'Sabu Barat'),
(5321, 53, 'Kab. Malaka', 2, 'Betun'),
(5371, 53, 'Kota Kupang', 1, 'Kupang'),
(6101, 61, 'Kab. Sambas', 2, 'Sambas'),
(6102, 61, 'Kab. Bengkayang', 2, 'Bengkayang'),
(6103, 61, 'Kab. Landak', 2, 'Ngabang'),
(6104, 61, 'Kab. Mempawah', 2, 'Mempawah'),
(6105, 61, 'Kab. Sanggau', 2, 'Sanggau'),
(6106, 61, 'Kab. Ketapang', 2, 'Ketapang'),
(6107, 61, 'Kab. Sintang', 2, 'Sintang'),
(6108, 61, 'Kab. Kapuas Hulu', 2, 'Putussibau'),
(6109, 61, 'Kab. Sekadau', 2, 'Sekadau'),
(6110, 61, 'Kab. Melawi', 2, 'Nanga Pinoh'),
(6111, 61, 'Kab. Kayong Utara', 2, 'Sukadane'),
(6112, 61, 'Kab. Kubu Raya', 2, 'Sungai Raya'),
(6171, 61, 'Kota Pontianak', 1, 'Pontianak'),
(6172, 61, 'Kota Singkawang', 1, 'Singkawang'),
(6201, 62, 'Kab. Kotawaringin Barat', 1, 'Pangkalan Bun'),
(6202, 62, 'Kab. Kotawaringin Timur', 1, 'Sampit'),
(6203, 62, 'Kab. Kapuas', 2, 'Kuala Kapuas'),
(6204, 62, 'Kab. Barito Selatan', 2, 'Buntok'),
(6205, 62, 'Kab. Barito Utara', 2, 'Muara Teweh'),
(6206, 62, 'Kab. Sukamara', 2, 'Sukamara'),
(6207, 62, 'Kab. Lamandau', 2, 'Nanga Bulik'),
(6208, 62, 'Kab. Seruyan', 2, 'Kuala Pembuang'),
(6209, 62, 'Kab. Katingan', 2, 'Kasongan'),
(6210, 62, 'Kab. Pulang Pisau', 2, 'Pulang Pisau'),
(6211, 62, 'Kab. Gunung Mas', 2, 'Kuala Kurun'),
(6212, 62, 'Kab. Barito Timur', 2, 'Tamiang Layang'),
(6213, 62, 'Kab. Murung Raya', 2, 'Puruk Cahu'),
(6271, 62, 'Kota Palangka Raya', 1, 'Palangkaraya'),
(6301, 63, 'Kab. Tanah Laut', 2, 'Pelaihari'),
(6302, 63, 'Kab. Kota Baru', 1, 'Kotabaru'),
(6303, 63, 'Kab. Banjar', 2, 'Martapura'),
(6304, 63, 'Kab. Barito Kuala', 2, 'Marabahan'),
(6305, 63, 'Kab. Tapin', 2, 'Rantau'),
(6306, 63, 'Kab. Hulu Sungai Selatan', 2, 'Kandangan'),
(6307, 63, 'Kab. Hulu Sungai Tengah', 2, 'Berabai'),
(6308, 63, 'Kab. Hulu Sungai Utara', 2, 'Amuntai'),
(6309, 63, 'Kab. Tabalong', 2, 'Tanjung'),
(6310, 63, 'Kab. Tanah Bumbu', 2, 'Batulicin'),
(6311, 63, 'Kab. Balangan', 2, 'Paringin'),
(6371, 63, 'Kota Banjarmasin', 1, 'Banjarmasin'),
(6372, 63, 'Kota Banjarbaru', 1, 'Banjarbaru'),
(6401, 64, 'Kab. Paser', 2, 'Tanah Grogot'),
(6402, 64, 'Kab. Kutai Barat', 2, 'Sendawar'),
(6403, 64, 'Kab. Kutai Kartanegara', 2, 'Tenggarong'),
(6404, 64, 'Kab. Kutai Timur', 2, 'Sanggatta'),
(6405, 64, 'Kab. Berau', 2, 'Tanjung Redeb'),
(6409, 64, 'Kab. Penajam Paser Utara', 2, 'Penajam'),
(6411, 64, 'Kab. Mahakam Ulu', 2, ''),
(6471, 64, 'Kota Balikpapan', 1, 'Balikpapan'),
(6472, 64, 'Kota Samarinda', 1, 'Samarinda'),
(6474, 64, 'Kota Bontang', 1, 'Bontang'),
(6501, 65, 'Kab. Malinau', 2, 'Malinau'),
(6502, 65, 'Kab. Bulungan', 2, 'Tanjung Selor'),
(6503, 65, 'Kab. Tana Tidung', 2, 'Tideng Pale'),
(6504, 65, 'Kab. Nunukan', 2, 'Nunukan'),
(6571, 65, 'Kota Tarakan', 1, 'Tarakan'),
(7101, 71, 'Kab. Bolaang Mongondow', 2, 'Lolak'),
(7102, 71, 'Kab. Minahasa', 2, 'Tondano'),
(7103, 71, 'Kab. Kepulauan Sangihe', 2, 'Tahuna'),
(7104, 71, 'Kab. Kepulauan Talaud', 2, 'Melongguane'),
(7105, 71, 'Kab. Minahasa Selatan', 2, 'Amurang'),
(7106, 71, 'Kab. Minahasa Utara', 2, 'Air Madidi'),
(7107, 71, 'Kab. Bolaang Mongondow Utara', 2, 'Boroko'),
(7108, 71, 'Kab. Siau Tagulandang Biaro', 2, ''),
(7109, 71, 'Kab. Minahasa Tenggara', 2, 'Ratahan'),
(7110, 71, 'Kab. Bolaang Mongondow Selatan', 2, 'Bolaang Uki'),
(7111, 71, 'Kab. Bolaang Mongondow Timur', 2, 'Tutuyan'),
(7171, 71, 'Kota Manado', 1, 'Manado'),
(7172, 71, 'Kota Bitung', 1, 'Bitung'),
(7173, 71, 'Kota Tomohon', 1, 'Tomohon'),
(7174, 71, 'Kota Kotamobagu', 1, 'Kotamobagu'),
(7201, 72, 'Kab. Banggai Kepulauan', 2, 'Salakan'),
(7202, 72, 'Kab. Banggai', 2, 'Luwuk'),
(7203, 72, 'Kab. Morowali', 2, 'Bungku'),
(7204, 72, 'Kab. Poso', 2, 'Poso'),
(7205, 72, 'Kab. Donggala', 2, 'Donggala'),
(7206, 72, 'Kab. Toli-Toli', 2, 'Toli Toli'),
(7207, 72, 'Kab. Buol', 2, 'Buol'),
(7208, 72, 'Kab. Parigi Moutong', 2, 'Parigi'),
(7209, 72, 'Kab. Tojo Una-Una', 2, 'Ampana'),
(7210, 72, 'Kab. Sigi', 2, 'Sigi Biromaru'),
(7211, 72, 'Kab. Banggai Laut', 2, ''),
(7271, 72, 'Kota Palu', 1, 'Palu'),
(7301, 73, 'Kab. Kepulauan Selayar', 2, 'Benteng'),
(7302, 73, 'Kab. Bulukumba', 2, 'Bulukumba'),
(7303, 73, 'Kab. Bantaeng', 2, 'Bantaeng'),
(7304, 73, 'Kab. Jeneponto', 2, 'Jeneponto'),
(7305, 73, 'Kab. Takalar', 2, 'Takalar'),
(7306, 73, 'Kab. Gowa', 2, 'Sunggu Minahasa'),
(7307, 73, 'Kab. Sinjai', 2, 'Sinjai'),
(7308, 73, 'Kab. Maros', 2, 'Maros'),
(7309, 73, 'Kab. Pangkajene Kepulauan', 2, 'Pangkep'),
(7310, 73, 'Kab. Barru', 2, 'Barru'),
(7311, 73, 'Kab. Bone', 2, 'Watampone'),
(7312, 73, 'Kab. Soppeng', 2, 'Watan Soppeng'),
(7313, 73, 'Kab. Wajo', 2, 'Sengkang'),
(7314, 73, 'Kab. Sidenreng Rappang', 2, 'Sidenreng'),
(7315, 73, 'Kab. Pinrang', 2, 'Pinrang'),
(7316, 73, 'Kab. Enrekang', 2, 'Enrekang'),
(7317, 73, 'Kab. Luwu', 2, 'Belopa'),
(7318, 73, 'Kab. Tana Toraja', 2, 'Makale'),
(7322, 73, 'Kab. Luwu Utara', 2, 'Masamba'),
(7325, 73, 'Kab. Luwu Timur', 2, 'Malili'),
(7326, 73, 'Kab. Tana Toraja Utara', 2, 'Rantepao'),
(7371, 73, 'Kota Makassar', 1, 'Makassar'),
(7372, 73, 'Kota Pare-Pare', 1, 'Pare Pare'),
(7373, 73, 'Kota Palopo', 1, 'Palopo'),
(7401, 74, 'Kab. Buton', 2, 'Pasar Wajo'),
(7402, 74, 'Kab. Muna', 2, 'Raha'),
(7403, 74, 'Kab. Konawe', 2, 'Unaaha'),
(7404, 74, 'Kab. Kolaka', 2, 'Kolaka'),
(7405, 74, 'Kab. Konawe Selatan', 2, 'Andoolo'),
(7406, 74, 'Kab. Bombana', 2, 'Rumbia'),
(7407, 74, 'Kab. Wakatobi', 2, 'Wangi Wangi'),
(7408, 74, 'Kab. Kolaka Utara', 2, 'Lasusua'),
(7409, 74, 'Kab. Buton Utara', 2, 'Buranga'),
(7410, 74, 'Kab. Konawe Utara', 2, 'Wanggudu'),
(7411, 74, 'Kab. Kolaka Timur', 2, ''),
(7471, 74, 'Kota Kendari', 1, 'Kendari'),
(7472, 74, 'Kota Baubau', 1, 'Bau Bau'),
(7501, 75, 'Kab. Boalemo', 2, 'Tilamuta'),
(7502, 75, 'Kab. Gorontalo', 2, 'Gorontalo'),
(7503, 75, 'Kab. Pohuwato', 2, 'Marisa'),
(7504, 75, 'Kab. Bone Bolango', 2, 'Suwawa'),
(7505, 75, 'Kab. Gorontalo Utara', 2, 'Kwandang'),
(7571, 75, 'Kota Gorontalo', 1, 'Gorontalo'),
(7601, 76, 'Kab. Majene', 2, 'Majene'),
(7602, 76, 'Kab. Polewali Mandar', 2, 'Polewali'),
(7603, 76, 'Kab. Mamasa', 2, 'Mamasa'),
(7604, 76, 'Kab. Mamuju', 2, 'Mamuju'),
(7605, 76, 'Kab. Mamuju Utara', 2, 'Pasangkayu'),
(7606, 76, 'Kab. Mamuju Tengah', 2, ''),
(8101, 81, 'Kab. Maluku Tenggara Barat', 2, 'Saumlaki'),
(8102, 81, 'Kab. Maluku Tenggara', 2, 'Kei Kecil'),
(8103, 81, 'Kab. Maluku Tengah', 2, 'Masohi'),
(8104, 81, 'Kab. Buru', 2, 'Namlea'),
(8105, 81, 'Kab. Kepulauan Aru', 2, 'Dobo'),
(8106, 81, 'Kab. Seram Bagian Barat', 2, 'Dataran Hunipopu'),
(8107, 81, 'Kab. Seram Bagian Timur', 2, 'Dataran Hunimoa'),
(8108, 81, 'Kab. Maluku Barat Daya', 2, 'Tiakur'),
(8109, 81, 'Kab. Buru Selatan', 2, 'Namrole'),
(8171, 81, 'Kota Ambon', 1, 'Ambon'),
(8172, 81, 'Kota Tual', 1, 'Tual'),
(8201, 82, 'Kab. Halmahera Barat', 2, 'Ternate'),
(8202, 82, 'Kab. Halmahera Tengah', 2, 'Weda'),
(8203, 82, 'Kab. Kepulauan Sula', 2, 'Sanana'),
(8204, 82, 'Kab. Halmahera Selatan', 2, 'Labuha'),
(8205, 82, 'Kab. Halmahera Utara', 2, 'Tobelo'),
(8206, 82, 'Kab. Halmahera Timur', 2, 'Maba'),
(8207, 82, 'Kab. Pulau Morotai', 2, 'Morotai Selatan'),
(8208, 82, 'Kab. Pulau Taliabu', 2, ''),
(8271, 82, 'Kota Ternate', 1, 'Ternate'),
(8272, 82, 'Kota Tidore Kepulauan', 1, 'Kota Tidore Kepula'),
(9101, 91, 'Kab. Fak-Fak', 2, 'Fak Fak'),
(9102, 91, 'Kab. Kaimana', 2, 'Kaimana'),
(9103, 91, 'Kab. Teluk Wondama', 2, 'Rasiei'),
(9104, 91, 'Kab. Teluk Bintuni', 2, 'Bintuni'),
(9105, 91, 'Kab. Manokwari', 2, 'Manokwari'),
(9106, 91, 'Kab. Sorong Selatan', 2, 'Teminabuan'),
(9107, 91, 'Kab. Sorong', 2, 'Sorong'),
(9108, 91, 'Kab. Raja Ampat', 2, 'Waisai'),
(9109, 91, 'Kab. Tambrauw', 2, 'Fef'),
(9110, 91, 'Kab. Maybrat', 2, 'Kumurkek'),
(9111, 91, 'Kab. Manokwari Selatan', 2, 'Ransiki'),
(9112, 91, 'Kab. Peg. Arfak', 2, 'Ulong'),
(9171, 91, 'Kota Sorong', 1, 'Sorong'),
(9401, 94, 'Kab. Merauke', 2, 'Merauke'),
(9402, 94, 'Kab. Jayawijaya', 2, 'Wamena'),
(9403, 94, 'Kab. Jayapura', 2, 'Jayapura'),
(9404, 94, 'Kab. Nabire', 2, 'Nabire'),
(9408, 94, 'Kab. Kepulauan Yapen', 2, 'Serui'),
(9409, 94, 'Kab. Biak Numfor', 2, 'Biak'),
(9410, 94, 'Kab. Paniai', 2, 'Enarotali'),
(9411, 94, 'Kab. Puncak Jaya', 2, 'Kota Mulia'),
(9412, 94, 'Kab. Mimika', 2, 'Timika'),
(9413, 94, 'Kab. Boven Digoel', 2, 'Tanah Merah'),
(9414, 94, 'Kab. Mappi', 2, 'Kepi'),
(9415, 94, 'Kab. Asmat', 2, 'Agats'),
(9416, 94, 'Kab. Yahukimo', 2, 'Sumohai'),
(9417, 94, 'Kab. Pegunungan Bintang', 2, 'Oksibil'),
(9418, 94, 'Kab. Tolikara', 2, 'Karubaga'),
(9419, 94, 'Kab. Sarmi', 2, 'Sarmi'),
(9420, 94, 'Kab. Keerom', 2, 'Waris'),
(9426, 94, 'Kab. Waropen', 2, 'Botawa'),
(9427, 94, 'Kab. Supiori', 2, 'Sorendiweri'),
(9428, 94, 'Kab. Memberamo Raya', 2, 'Burmeso'),
(9429, 94, 'Kab. Nduga', 2, 'Kenyam'),
(9430, 94, 'Kab. Lanny Jaya', 2, 'Tiom'),
(9431, 94, 'Kab. Mamberamo Tengah', 2, 'Kobakma'),
(9432, 94, 'Kab. Yalimo', 2, 'Elelim'),
(9433, 94, 'Kab. Puncak', 2, 'Ilaga'),
(9434, 94, 'Kab. Dogiyai', 2, 'Kigamani'),
(9435, 94, 'Kab. Intan Jaya', 2, 'Sugapa'),
(9436, 94, 'Kab. Deiyai', 2, 'Tigi'),
(9471, 94, 'Kota Jayapura', 1, 'Jayapura');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_07_09_152657_create_sliders_table', 1),
(5, '2020_07_09_152749_create_params_table', 1),
(6, '2020_07_09_152802_create_provinsi_table', 1),
(7, '2020_07_09_152819_create_kab_kota_table', 1),
(8, '2020_07_09_152834_create_packages_table', 1),
(9, '2020_07_09_152846_create_forms_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `packages`
--

CREATE TABLE `packages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` enum('true','false') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `packages`
--

INSERT INTO `packages` (`id`, `image`, `title`, `package_name`, `slug`, `content`, `active`, `created_at`, `updated_at`) VALUES
(1, 'JAjUNbIAm1QUpghMDDT0.jpg', 'Title edit', 'Internet Indihome Super edit', 'title-edit', '<p><strong>Harga Indihome FIT Berkah 2020 3 play (Internet + Useetv + Telpon) :</strong></p>\r\n\r\n<ul>\r\n	<li>10 Mbps = Rp. 360.000,-/Bulan</li>\r\n	<li>20 Mbps = Rp. 395.000,-/Bulan</li>\r\n	<li>30 Mbps = Rp. 480.000,-/Bulan</li>\r\n	<li>40 Mbps = Rp. 560.000,-/Bulan</li>\r\n	<li>50 Mbps = Rp. 625.000,-/Bulan</li>\r\n</ul>\r\n\r\n<h4><strong>Keterangan :</strong></h4>\r\n\r\n<ul>\r\n	<li>\r\n	<ul>\r\n		<li>Paket UseeTV Entry terdiri dari 85 channel (85 SD + 8 HD) 93 channel</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<ul>\r\n		<li>Khusus untuk IndiHome Paket Fantastic Deal diberlakukan Fair Usage Policy (FUP) mulai penggunaan 400 GB.</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<ul>\r\n		<li>Biaya Pasang Baru (PSB) IndiHome Rp100.000 akan ditagihkan pada bulan pertama saja dan tidak diperkenankan melakukan pembayaran secara tunai selain di Plasa Telkom.</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<ul>\r\n		<li>Masa promo paket ini berlaku sejak komersialisasi sampai dengan 31Januari 2020.</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<ul>\r\n		<li>Harga di atas berlaku selama pelanggan berlangganan IndiHome Paket Merdeka.</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<ul>\r\n		<li>Telkom menyediakan Hybrid Box IndiHome dan ONT selama berlangganan.</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<ul>\r\n		<li>Untuk pelanggan yang ingin memasang Hybrid Box IndiHome tambahan akan dikenakan biaya tambahan bulanan di mana Telkom menyediakan Hybrid Box yang dimaksud. Pelanggan bisa dikenakan biaya penarikan jaringan tambahan yang dibayarkan kepada mitra penarikan jaringan.</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>\r\n	<ul>\r\n		<li>Harga belum termasuk PPN 10%.</li>\r\n	</ul>\r\n	</li>\r\n</ul>\r\n\r\n<ul>\r\n	<li>Syarat &amp; ketentuan berlaku.</li>\r\n</ul>', 'true', '2020-07-20 23:23:00', '2020-07-20 23:23:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `params`
--

CREATE TABLE `params` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_param` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `param` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` enum('true','false') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `params`
--

INSERT INTO `params` (`id`, `category_param`, `param`, `active`) VALUES
(1, 'contact', '089657341120', 'true'),
(2, 'address', 'Citayam kp kelapa Desa rawa panjang', 'true'),
(3, 'email', 'ipulbelcram@gmail.com', 'true');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE `provinsi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provinsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`id`, `provinsi`) VALUES
(11, 'Aceh'),
(12, 'Sumatera Utara'),
(13, 'Sumatera Barat'),
(14, 'Riau'),
(15, 'Jambi'),
(16, 'Sumatera Selatan'),
(17, 'Bengkulu'),
(18, 'Lampung'),
(19, 'Bangka Belitung'),
(21, 'Kepulauan Riau'),
(31, 'D.K.I. Jakarta'),
(32, 'Jawa Barat'),
(33, 'Jawa Tengah'),
(34, 'D.I. Yogyakarta'),
(35, 'Jawa Timur'),
(36, 'Banten'),
(51, 'Bali'),
(52, 'Nusa Tenggara Barat'),
(53, 'Nusa Tenggara Timur'),
(61, 'Kalimantan Barat'),
(62, 'Kalimantan Tengah'),
(63, 'Kalimantan Selatan'),
(64, 'Kalimantan Timur'),
(65, 'Kalimantan Utara'),
(71, 'Sulawesi Utara'),
(72, 'Sulawesi Tengah'),
(73, 'Sulawesi Selatan'),
(74, 'Sulawesi Tenggara'),
(75, 'Gorontalo'),
(76, 'Sulawesi Barat'),
(81, 'Maluku'),
(82, 'Maluku Utara'),
(91, 'Papua Barat'),
(94, 'Papua');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `title`, `order`) VALUES
(1, 'cJiuwtQsdYemJnRc9BGg.jpg', 'Title', 1),
(2, 'fy9fdQ8LHKUpuH2RFRCx.jpg', 'Title 2', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'syaiful', 'Ahmad Syaiful Akbar', 'ipulbelcram@gmail.com', NULL, '$2y$10$8WfWGCb6U1Qd.5/5K7HpG.vcYVpv.X4JXqZ0kpEUyssXtdBhU7JP2', NULL, '2020-07-20 23:13:39', '2020-07-20 23:13:39');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forms_package_id_foreign` (`package_id`),
  ADD KEY `forms_provinsi_id_foreign` (`provinsi_id`),
  ADD KEY `forms_kab_kota_id_foreign` (`kab_kota_id`);

--
-- Indeks untuk tabel `kab_kota`
--
ALTER TABLE `kab_kota`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kab_kota_provinsi_id_foreign` (`provinsi_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `packages_slug_unique` (`slug`);

--
-- Indeks untuk tabel `params`
--
ALTER TABLE `params`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sliders_order_unique` (`order`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `forms`
--
ALTER TABLE `forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `kab_kota`
--
ALTER TABLE `kab_kota`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9472;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `packages`
--
ALTER TABLE `packages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `params`
--
ALTER TABLE `params`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT untuk tabel `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `forms`
--
ALTER TABLE `forms`
  ADD CONSTRAINT `forms_kab_kota_id_foreign` FOREIGN KEY (`kab_kota_id`) REFERENCES `kab_kota` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forms_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `forms_provinsi_id_foreign` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `kab_kota`
--
ALTER TABLE `kab_kota`
  ADD CONSTRAINT `kab_kota_provinsi_id_foreign` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
