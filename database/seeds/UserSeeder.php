<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create( [
            'id' => 1,
            'username' => 'syaiful',
            'name' => 'Ahmad Syaiful Akbar',
            'email' => 'ipulbelcram@gmail.com',
            'password' => '$2y$10$8WfWGCb6U1Qd.5/5K7HpG.vcYVpv.X4JXqZ0kpEUyssXtdBhU7JP2',
            'created_at' => '2020-07-21 06:13:39',
            'updated_at' => '2020-07-21 06:13:39'
        ]);
    }
}
