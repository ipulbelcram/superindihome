<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('params')->insert([
            'category_param' => 'alamat',
            'param' => 'asd',
            'active' => 'true'
        ]);

        DB::table('params')->insert([
            'category_param' => 'contact',
            'param' => '089657341120',
            'active' => 'true'
        ]);
    }
}
